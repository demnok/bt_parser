var csv = require('csv-parser')
var fs = require('fs')

var parsed = {};
const reducer = (accumulator, currentValue) => parseFloat(accumulator) + parseFloat(currentValue);

fs.createReadStream('budget.csv')
  .pipe(csv())
  .on('data', function (data) {
    var regex = /TID:[a-zA-Z0-9]* [a-zA-Z ]* [0-9]*/g;
    if (data.Descriere.match(regex)) {
      var company = data.Descriere.match(regex)[0].replace(/[0-9]*/g, '');
      if(!parsed[company]) { parsed[company] = []; }

      parsed[company].push(data.Debit);
    }
  }).on('end', function() {
    for(var key in parsed) {
      console.log(key, parsed[key].reduce(reducer));
    }
  });

